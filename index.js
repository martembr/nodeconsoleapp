const fs = require('fs');
const os = require('os');
const http = require('http');


// start menu
menu();

function menu() {
    // while loop - read in from terminal 
    displayOSInfo();
    readAndDisplayFile("/package.json")

}

function menuOptions(choice) {
    // switch case
}


function readAndDisplayFile(filename) {
    fs.readFile(__dirname + filename, 'utf-8', (err, content) => {
        console.log(content);
    });
}

function displayOSInfo() {
    // memory
    let mem = (os.totalmem()/1024/1024/1024).toFixed(2);
    let freemem = (os.freemem()/1024/1024/1024).toFixed(2);
    // system memory
    console.log(`SYSTEM MEMORY: ${mem} GB`);
    // free memory
    console.log(`FREE MEMORY: ${freemem} GB`);
    // cores
    console.log(`CPUs: ${os.cpus().length}`);
    // arch
    console.log(`Arch: ${os.arch()}`);
    // platform
    console.log(`PLATFORM: ${os.platform()}`);
    // user
    console.log(`USER: ${os.userInfo().username}`);
}

function startHTTPServer() {
    // create server
    const server = http.createServer( (req, res) => {
        if (req.url == "/") {
            res.write("Hello World");
            res.end();
        }
    });

    // listen on port 3000
    server.listen(3000);
}